import * as child_process from 'child_process';
import {blue, yellow} from 'chalk';
import {getGlobalVariable} from './env';
import {rimraf, writeFile} from './fs';
const treeKill = require('tree-kill');


interface ExecOptions {
  silent?: boolean;
  waitForMatch?: RegExp;
}